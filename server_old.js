// Modules imports
const mysql = require('mysql');
const express = require('express');
const bodyparser = require('body-parser');
var cors = require('cors');
var morgan = require('morgan');

// server config instantiation
var app = express();
app.use(bodyparser.json());
app.use(cors())
app.use(morgan('combined'));

// bd Connection declaration
var mysqlConnectionWebsite = mysql.createConnection({
    host: 'localhost',
    user: 'swetboy225',
    password: 'ARIELUS225@a',
    database: 'esma_bd'
});

var mysqlConnectionApps = mysql.createConnection({
    host: 'localhost',
    user: 'swetboy225',
    password: 'ARIELUS225@a',
    database: 'apps_'
});

var mysqlConnectionAnnexe = mysql.createConnection({
    host: 'localhost',
    user: 'swetboy225',
    password: 'ARIELUS225@a',
    database: 'ESMASmartSchoolAppDB'
});

// DB Connection test
mysqlConnectionWebsite.connect((err) => {
    if (!err)
        console.log(' Website DB connection succeded.');
    else
        console.log(' Website DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

mysqlConnectionApps.connect((err) => {
    if (!err)
        console.log(' Logic campus DB connection succeded.');
    else
        console.log(' Logic campus DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

// Start Server
app.listen(3001, () => console.log('ESMA School Mobile App server is runnig at port no : 3001'));

// ------ Endpoint for website ------ //
app.get('/testWebsite/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM test', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Get all news
app.get('/news/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM actualites ORDER BY actualites.date DESC ', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get news by id
app.get('/new/:id', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM actualites WHERE id_actualites = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get all event
app.get('/agenda/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM agenda ORDER BY agenda.date DESC', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get event by id
app.get('/agenda/:id', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM agenda WHERE id_agenda = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows[0]);
        else
            console.log(err);
    })
});


//Get all students news
app.get('/students-news/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM estudiantine ORDER BY estudiantine.date DESC', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get students news details
app.get('/students-new/:id', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM estudiantine WHERE id_estudiantine = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })

});

//Get all event
app.get('/events/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM evenement ORDER BY evenement.date DESC', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get event by id
app.get('/event/:id', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM evenement WHERE id_evenement = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows[0]);
        else
            console.log(err);
    })
});

//Get all  school contacts
app.get('/school-contacts/', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM schoolContacts', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get school contact by id
app.get('/school-contact/:id', (req, res) => {
    mysqlConnectionWebsite.query('SELECT * FROM schoolContacts WHERE idContact = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows[0]);
        else
            console.log(err);
    })
});
// ------ Endpoint for Annex DB------ //
// test Annexe DB
app.get('/testAnnexe/', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM test', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Get all  Flash infos
app.get('/flashInfos/:idClass', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM flashInfos WHERE classIdFlashInfo = ?',[req.params.idClass], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
            console.log(req.params.idClass)
    })
});

//Get Flash info by id
app.get('/flashInfo/:id', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM flashInfos WHERE idFlashInfo + ?',[req.params.idClass], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
            console.log(req.params.idClass)
    })
});
//Get all notifications for student id
app.get('/notifications/:id', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM notifications WHERE studentIdNotifications = ?',[req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get notification by id
app.get('/notification/:id', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM notifications WHERE idNotifications = ?',[req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get all quiz for student id
app.get('/quiz-date/:id', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT * FROM quizDate WHERE classIdQuizDate = ? ORDER BY quizDate.dateQuizDate',[req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

// Get student attendances
app.get('/absence/:id', (req, res) => {
    mysqlConnectionAnnexe.query('SELECT absence.date, emploitemps.jour, etudiant.numeroMatricule, plageheure.heureDebut, plageheure.heureFin, matiere.libelle, TIMEDIFF(plageheure.heureFin,plageheure.heureDebut) FROM absence, emploitemps, etudiant, matiere, plageheure, programmecours, matiereniveau WHERE absence.idEtudiant = etudiant.idEtudiant AND absence.idEmploiTemps = emploitemps.idEmploiTemps AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere WHERE absence.idEtudiant = ?',[req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

// ------ Endpoint for Logic campus ------ //
// test logic campus DB
app.get('/testLogic/', (req, res) => {
    mysqlConnectionApps.query('SELECT * FROM test', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Get student by matricule
app.get('/student/:id', (req, res) => {
    mysqlConnectionApps.query('SELECT etudiant.idEtudiant, classe.idClasse, etudiant.numeroMatricule, etudiant.nom, etudiant.prenoms, etudiant.sexe, etudiant.dateNaissance, etudiant.lieuNaissance, etudiant.adresse, etudiant.telephone, etudiant.email, classe.libelle, etudiant.photo, etudiantclasse.dateEnregistrement, parents.nom, parents.prenoms, etudiantparent.titre, parents.telephone FROM etudiant, classe, etudiantparent, etudiantclasse, parents WHERE etudiantclasse.idEtudiant = etudiant.idEtudiant AND etudiantclasse.idClasse = classe.idClasse AND etudiantparent.idEtudiant = etudiant.idEtudiant AND etudiantparent.idParent = parents.idParent AND etudiant.numeroMatricule = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows[0]);
        else
            console.log(err);
    })
});

// Get timetable by class Day = monday
app.get('/timetable-monday/:id/', (req, res) => {
    mysqlConnectionApps.query('SELECT classe.idClasse, classe.libelle, emploitemps.jour, plageheure.heureDebut, plageheure.heureFin, salle.libelle, enseignant.nom, enseignant.prenoms, matiere.libelle FROM emploitemps, programmecours, plageheure, salle, enseignant, matiereniveau, matiere, classe WHERE emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idSalle = salle.idSalle AND programmecours.idEnseignant = enseignant.idEnseignant AND programmecours.idClasse = classe.idClasse AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere AND classe.idClasse = ? AND emploitemps.jour = 1', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows);
        else
            console.log(err);
    })
});

// Get timetable by class Day = tuesday
app.get('/timetable-tuesday/:id/', (req, res) => {
    mysqlConnectionApps.query('SELECT classe.idClasse, classe.libelle, emploitemps.jour, plageheure.heureDebut, plageheure.heureFin, salle.libelle, enseignant.nom, enseignant.prenoms, matiere.libelle FROM emploitemps, programmecours, plageheure, salle, enseignant, matiereniveau, matiere, classe WHERE emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idSalle = salle.idSalle AND programmecours.idEnseignant = enseignant.idEnseignant AND programmecours.idClasse = classe.idClasse AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere AND classe.idClasse = ? AND emploitemps.jour = 2', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows);
        else
            console.log(err);
    })
});

// Get timetable by class Day = wednesday
app.get('/timetable-wednesday/:id/', (req, res) => {
    mysqlConnectionApps.query('SELECT classe.idClasse, classe.libelle, emploitemps.jour, plageheure.heureDebut, plageheure.heureFin, salle.libelle, enseignant.nom, enseignant.prenoms, matiere.libelle FROM emploitemps, programmecours, plageheure, salle, enseignant, matiereniveau, matiere, classe WHERE emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idSalle = salle.idSalle AND programmecours.idEnseignant = enseignant.idEnseignant AND programmecours.idClasse = classe.idClasse AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere AND classe.idClasse = ? AND emploitemps.jour = 3', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows);
        else
            console.log(err);
    })
});

// Get timetable by class Day = thursday
app.get('/timetable-thursday/:id/', (req, res) => {
    mysqlConnectionApps.query('SELECT classe.idClasse, classe.libelle, emploitemps.jour, plageheure.heureDebut, plageheure.heureFin, salle.libelle, enseignant.nom, enseignant.prenoms, matiere.libelle FROM emploitemps, programmecours, plageheure, salle, enseignant, matiereniveau, matiere, classe WHERE emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idSalle = salle.idSalle AND programmecours.idEnseignant = enseignant.idEnseignant AND programmecours.idClasse = classe.idClasse AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere AND classe.idClasse = ? AND emploitemps.jour = 4', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows);
        else
            console.log(err);
    })
});

// Get timetable by class Day = friday
app.get('/timetable-friday/:id/', (req, res) => {
    mysqlConnectionApps.query('SELECT classe.idClasse, classe.libelle, emploitemps.jour, plageheure.heureDebut, plageheure.heureFin, salle.libelle, enseignant.nom, enseignant.prenoms, matiere.libelle FROM emploitemps, programmecours, plageheure, salle, enseignant, matiereniveau, matiere, classe WHERE emploitemps.idProgrammeCours = programmecours.idProgrammeCours AND emploitemps.idPlageHeure = plageheure.idPlageHeure AND emploitemps.idSalle = salle.idSalle AND programmecours.idEnseignant = enseignant.idEnseignant AND programmecours.idClasse = classe.idClasse AND programmecours.idMatiereNiveau = matiereniveau.idMatiereNiveau AND matiereniveau.idMatiere = matiere.idMatiere AND classe.idClasse = ? AND emploitemps.jour = 5', [req.params.id], (err, rows, fields) => {
        if (!err)
        res.status(200).json(rows);
        else
            console.log(err);
    })
});